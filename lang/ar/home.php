<?php

return [


    'home' => 'الصفحة الرئيسية',
    'services' => 'خدماتنا',
    'gallery' => 'المعرض',
    'features' => 'المميزات',
    'award' => 'الإعلانات',
    'pricing' => 'الأسعار',
    'faq' => 'الأسئلة المكررة',
    'Our_services' => 'خدماتنا',
    'Our_gallery' => 'المعرض',
    'Amazing_features' => 'المميزات',
    'Frequently' => 'الأسئلة المكررة',
    'Follw_us' => 'تابعونا على :',
    'Phone_number' => 'رقم الهاتف',
    'Fast_links' => 'الروابط السريعة',


];
