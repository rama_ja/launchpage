<!--  BEGIN SIDEBAR  -->
<div class="sidebar-wrapper sidebar-theme">

    <nav id="sidebar">

        <div class="shadow-bottom"></div>
        <ul class="list-unstyled menu-categories" id="accordionExample">
            @if($admin->type=='1')
                <li class="menu {{ ( (request()->is('*/users*'))|| (request()->is('*/admins*')) )? 'active' : '' }}">
                    <a href="#dashboard" data-bs-toggle="collapse" aria-expanded="{{( (request()->is('*/users*')) || (request()->is('*/admins*')) ) ? 'true' : 'false' }}" class="dropdown-toggle">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-key"><path d="M21 2l-2 2m-7.61 7.61a5.5 5.5 0 1 1-7.778 7.778 5.5 5.5 0 0 1 7.777-7.777zm0 0L15.5 7.5m0 0l3 3L22 7l-3-3m-3.5 3.5L19 4"></path></svg>
                            <span>{{trans('sidebar.all_users')}}</span>
                        </div>
                        <div>
                            <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                        </div>
                    </a>
                    <ul class="collapse submenu list-unstyled {{  ( (request()->is('*/users*'))  || (request()->is('*/admins*')) ) ? 'show' : ''}}" id="dashboard" data-bs-parent="#accordionExample">
                        <li class="{{ (request()->is('*/admins*')) ? 'active' : '' }}">
                            <a href="{{route('admin_panel.admins.index')}}"> {{trans('sidebar.admins')}} </a>
                        </li>
                    </ul>
                    <ul class="collapse submenu list-unstyled {{ ( (request()->is('*/users*')) || (request()->is('*/admins*')) ) ? 'show' : ''}}" id="dashboard" data-bs-parent="#accordionExample">
                        <li class="{{  (request()->is('*/users*')) ? 'active' : '' }}">
                            <a href="/admin_panel/users"> {{trans('sidebar.users')}} </a>
                        </li>
                    </ul>
                </li>
            @else
                <li class="menu {{ (request()->is('*/users*')) || (request()->is('*/users')) ? 'active' : '' }}">
                    <a href="/admin_panel/users" aria-expanded="false" class="dropdown-toggle">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-key"><path d="M21 2l-2 2m-7.61 7.61a5.5 5.5 0 1 1-7.778 7.778 5.5 5.5 0 0 1 7.777-7.777zm0 0L15.5 7.5m0 0l3 3L22 7l-3-3m-3.5 3.5L19 4"></path></svg>
                            <span> {{trans('sidebar.users')}} </span>
                        </div>
                    </a>
                </li>
            @endif
            @if($admin->type=='1')
                <li class="menu {{ ( (request()->is('*/settings*'))|| (request()->is('*/metas*'))|| (request()->is('*/sections*')) )? 'active' : '' }}">
                    <a href="#settings" data-bs-toggle="collapse" aria-expanded="{{( (request()->is('*/settings*')) || (request()->is('*/metas*'))|| (request()->is('*/sections*')) ) ? 'true' : 'false' }}" class="dropdown-toggle">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-settings"><circle cx="12" cy="12" r="3"></circle><path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path></svg>
                            <span>{{trans('sidebar.general_settings')}} </span>
                        </div>
                        <div>
                            <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                        </div>
                    </a>
                    <ul class="collapse submenu list-unstyled {{  ( (request()->is('*/settings*'))  || (request()->is('*/metas*')) || (request()->is('*/sections*')) ) ? 'show' : ''}}" id="settings" data-bs-parent="#accordionExample">
                        <li class="{{ (request()->is('*/settings*')) ? 'active' : '' }}">
                            <a href="{{route('admin_panel.settings.index')}}"> {{trans('sidebar.appearance')}} </a>
                        </li>
                    </ul>
                    <ul class="collapse submenu list-unstyled {{ ( (request()->is('*/settings*')) || (request()->is('*/metas*')) || (request()->is('*/sections*')) ) ? 'show' : ''}}" id="settings" data-bs-parent="#accordionExample">
                        <li class="{{  (request()->is('*/metas*')) ? 'active' : '' }}">
                            <a href="{{route('admin_panel.metas.index')}}"> {{trans('sidebar.settings')}}  </a>
                        </li>
                    </ul>
                    <ul class="collapse submenu list-unstyled {{ ( (request()->is('*/settings*')) || (request()->is('*/metas*'))|| (request()->is('*/sections*')) ) ? 'show' : ''}}" id="settings" data-bs-parent="#accordionExample">
                        <li class="{{  (request()->is('*/sections*')) ? 'active' : '' }}">
                            <a href="{{route('admin_panel.sections.index')}}"> {{trans('sidebar.sections')}}  </a>
                        </li>
                    </ul>
                </li>

            @endif

            <li class="menu {{ ( (request()->is('*/services*'))|| (request()->is('*/gallery*'))|| (request()->is('*/features*')) || (request()->is('*/sliders*'))|| (request()->is('*/questions*'))|| (request()->is('*/prices*'))|| (request()->is('*/price_text*')) || (request()->is('*/edit_whatsapp'))|| (request()->is('*/edit_call')) )? 'active' : '' }}">
                <a href="#cont" data-bs-toggle="collapse" aria-expanded="{{( (request()->is('*/services*')) || (request()->is('*/gallery*'))|| (request()->is('*/features*')) || (request()->is('*/sliders*'))|| (request()->is('*/questions*'))|| (request()->is('*/prices*'))|| (request()->is('*/price_text*')) || (request()->is('*/edit_whatsapp'))|| (request()->is('*/edit_call')) ) ? 'true' : 'false' }}" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-archive"><polyline points="21 8 21 21 3 21 3 8"></polyline><rect x="1" y="3" width="22" height="5"></rect><line x1="10" y1="12" x2="14" y2="12"></line></svg>
                        <span>{{trans('sidebar.content')}} </span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled {{  ( (request()->is('*/services*'))  || (request()->is('*/gallery*')) || (request()->is('*/features*')) || (request()->is('*/sliders*'))|| (request()->is('*/questions*'))|| (request()->is('*/prices*'))|| (request()->is('*/price_text*')) || (request()->is('*/edit_whatsapp'))|| (request()->is('*/edit_call')) ) ? 'show' : ''}}" id="cont" data-bs-parent="#accordionExample">
                    <li class="{{ (request()->is('*/services*')) ? 'active' : '' }}">
                        <a href="{{route('admin_panel.services.index')}}"> {{trans('sidebar.services')}} </a>
                    </li>
                </ul>
                <ul class="collapse submenu list-unstyled {{ ( (request()->is('*/services*')) || (request()->is('*/gallery*')) || (request()->is('*/features*')) || (request()->is('*/sliders*'))|| (request()->is('*/questions*'))|| (request()->is('*/prices*'))|| (request()->is('*/price_text*')) || (request()->is('*/edit_whatsapp'))|| (request()->is('*/edit_call')) ) ? 'show' : ''}}" id="cont" data-bs-parent="#accordionExample">
                    <li class="{{  (request()->is('*/gallery*')) ? 'active' : '' }}">
                        <a href="{{route('admin_panel.gallery.index')}}"> {{trans('sidebar.gallery')}}  </a>
                    </li>
                </ul>
                <ul class="collapse submenu list-unstyled {{ ( (request()->is('*/services*')) || (request()->is('*/gallery*'))|| (request()->is('*/features*')) || (request()->is('*/sliders*'))|| (request()->is('*/questions*'))|| (request()->is('*/prices*'))|| (request()->is('*/price_text*')) || (request()->is('*/edit_whatsapp'))|| (request()->is('*/edit_call')) ) ? 'show' : ''}}" id="cont" data-bs-parent="#accordionExample">
                    <li class="{{  (request()->is('*/features*')) ? 'active' : '' }}">
                        <a href="{{route('admin_panel.features.index')}}"> {{trans('sidebar.features')}}  </a>
                    </li>
                </ul>
                <ul class="collapse submenu list-unstyled {{ ( (request()->is('*/services*')) || (request()->is('*/gallery*'))|| (request()->is('*/features*'))|| (request()->is('*/sliders*')) || (request()->is('*/questions*'))|| (request()->is('*/prices*'))|| (request()->is('*/price_text*')) || (request()->is('*/edit_whatsapp'))|| (request()->is('*/edit_call')) ) ? 'show' : ''}}" id="cont" data-bs-parent="#accordionExample">
                    <li class="{{  (request()->is('*/sliders*')) ? 'active' : '' }}">
                        <a href="{{route('admin_panel.sliders.index')}}"> {{trans('sidebar.award')}}  </a>
                    </li>
                </ul>
                <ul class="collapse submenu list-unstyled {{ ( (request()->is('*/services*')) || (request()->is('*/gallery*'))|| (request()->is('*/features*'))|| (request()->is('*/sliders*'))|| (request()->is('*/questions*')) || (request()->is('*/prices*'))|| (request()->is('*/price_text*'))|| (request()->is('*/edit_whatsapp'))|| (request()->is('*/edit_call')) ) ? 'show' : ''}}" id="cont" data-bs-parent="#accordionExample">
                    <li class="{{  (request()->is('*/questions*')) ? 'active' : '' }}">
                        <a href="{{route('admin_panel.questions.index')}}"> {{trans('sidebar.frequently_questions')}}  </a>
                    </li>
                </ul>
                <ul class="collapse submenu list-unstyled {{ ( (request()->is('*/services*')) || (request()->is('*/gallery*'))|| (request()->is('*/features*'))|| (request()->is('*/sliders*'))|| (request()->is('*/questions*'))|| (request()->is('*/prices*'))|| (request()->is('*/price_text*'))|| (request()->is('*/edit_whatsapp'))|| (request()->is('*/edit_call')) ) ? 'show' : ''}}" id="cont" data-bs-parent="#accordionExample">
                    <li class="{{  (request()->is('*/prices*')) ? 'active' : '' }}">
                        <a href="{{route('admin_panel.prices.index')}}"> {{trans('sidebar.prices')}}  </a>
                    </li>
                </ul>
                <ul class="collapse submenu list-unstyled {{ ( (request()->is('*/services*')) || (request()->is('*/gallery*'))|| (request()->is('*/features*'))|| (request()->is('*/sliders*'))|| (request()->is('*/questions*'))|| (request()->is('*/prices*'))|| (request()->is('*/price_text*')) || (request()->is('*/edit_whatsapp'))|| (request()->is('*/edit_call')) ) ? 'show' : ''}}" id="cont" data-bs-parent="#accordionExample">
                    <li class="{{  (request()->is('*/price_text*')) ? 'active' : '' }}">
                        <a href="{{route('admin_panel.price_text.index')}}"> {{trans('sidebar.prices_text')}}  </a>
                    </li>
                </ul>
                <ul class="collapse submenu list-unstyled {{ ( (request()->is('*/services*')) || (request()->is('*/gallery*'))|| (request()->is('*/features*'))|| (request()->is('*/sliders*'))|| (request()->is('*/questions*'))|| (request()->is('*/prices*'))|| (request()->is('*/price_text*'))|| (request()->is('*/edit_whatsapp'))|| (request()->is('*/edit_call')) ) ? 'show' : ''}}" id="cont" data-bs-parent="#accordionExample">
                    <li class="{{  (request()->is('*/edit_whatsapp')) ? 'active' : '' }}">
                        <a href="/admin_panel/edit_whatsapp"> WhatsApp  </a>
                    </li>
                </ul>
                <ul class="collapse submenu list-unstyled {{ ( (request()->is('*/services*')) || (request()->is('*/gallery*'))|| (request()->is('*/features*'))|| (request()->is('*/sliders*'))|| (request()->is('*/questions*'))|| (request()->is('*/prices*'))|| (request()->is('*/price_text*'))|| (request()->is('*/edit_whatsapp'))|| (request()->is('*/edit_call')) ) ? 'show' : ''}}" id="cont" data-bs-parent="#accordionExample">
                    <li class="{{  (request()->is('*/edit_call')) ? 'active' : '' }}">
                        <a href="/admin_panel/edit_call"> Call flyer  </a>
                    </li>
                </ul>
            </li>

            <li class="menu {{ ( (request()->is('*/edit_phone'))|| (request()->is('*/edit_mail'))  )? 'active' : '' }}">
                <a href="#com" data-bs-toggle="collapse" aria-expanded="{{( (request()->is('*/edit_phone'))|| (request()->is('*/edit_mail')) ) ? 'true' : 'false' }}" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-phone-call"><path d="M15.05 5A5 5 0 0 1 19 8.95M15.05 1A9 9 0 0 1 23 8.94m-1 7.98v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path></svg>
                        <span>{{trans('sidebar.comunication')}} </span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled {{ ( (request()->is('*/edit_phone'))|| (request()->is('*/edit_mail')) ) ? 'show' : ''}}" id="com" data-bs-parent="#accordionExample">
                    <li class="{{  (request()->is('*/edit_phone')) ? 'active' : '' }}">
                        <a href="/admin_panel/edit_phone"> Phone number </a>
                    </li>
                </ul>

                <ul class="collapse submenu list-unstyled {{ ( (request()->is('*/edit_phone*'))|| (request()->is('*/edit_mail')) ) ? 'show' : ''}}" id="com" data-bs-parent="#accordionExample">
                    <li class="{{  (request()->is('*/edit_mail')) ? 'active' : '' }}">
                        <a href="/admin_panel/edit_mail"> {{trans('sidebar.mail')}}  </a>
                    </li>
                </ul>

            </li>


{{--            <li class="menu {{ (request()->is('*/services*')) ? 'active' : '' }}">--}}
{{--                <a href="{{route('admin_panel.services.index')}}" aria-expanded="false" class="dropdown-toggle">--}}
{{--                    <div class="">--}}
{{--                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trello"><rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect><rect x="7" y="7" width="3" height="9"></rect><rect x="14" y="7" width="3" height="5"></rect></svg>--}}
{{--                        <span> {{trans('sidebar.services')}}  </span>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class="menu {{ (request()->is('*/gallery*')) ? 'active' : '' }}">--}}
{{--                <a href="{{route('admin_panel.gallery.index')}}" aria-expanded="false" class="dropdown-toggle">--}}
{{--                    <div class="">--}}
{{--                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-camera"><path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></path><circle cx="12" cy="13" r="4"></circle></svg>--}}
{{--                        <span> {{trans('sidebar.gallery')}}  </span>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class="menu {{ (request()->is('*/features*')) ? 'active' : '' }}">--}}
{{--                <a href="{{route('admin_panel.features.index')}}" aria-expanded="false" class="dropdown-toggle">--}}
{{--                    <div class="">--}}
{{--                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>--}}
{{--                        <span> {{trans('sidebar.features')}} </span>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class="menu {{ (request()->is('*/sliders*')) ? 'active' : '' }}">--}}
{{--                <a href="{{route('admin_panel.sliders.index')}}" aria-expanded="false" class="dropdown-toggle">--}}
{{--                    <div class="">--}}
{{--                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-airplay"><path d="M5 17H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-1"></path><polygon points="12 15 17 21 7 21 12 15"></polygon></svg>--}}
{{--                        <span>{{trans('sidebar.award')}} </span>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class="menu {{ (request()->is('*/questions*')) ? 'active' : '' }}">--}}
{{--                <a href="{{route('admin_panel.questions.index')}}" aria-expanded="false" class="dropdown-toggle">--}}
{{--                    <div class="">--}}
{{--                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-3"><path d="M12 20h9"></path><path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path></svg>--}}
{{--                        <span>{{trans('sidebar.frequently_questions')}} </span>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </li>--}}

            <li class="menu {{ (request()->is('*/media*')) ? 'active' : '' }}">
                <a href="{{route('admin_panel.media.edit',1)}}" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-tv"><rect x="2" y="7" width="20" height="15" rx="2" ry="2"></rect><polyline points="17 2 12 7 7 2"></polyline></svg>
                        <span>  {{trans('sidebar.social_media')}}  </span>
                    </div>
                </a>
            </li>

{{--            <li class="menu {{ (request()->is('*/prices*')) ? 'active' : '' }}">--}}
{{--                <a href="{{route('admin_panel.prices.index')}}" aria-expanded="false" class="dropdown-toggle">--}}
{{--                    <div class="">--}}
{{--                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trending-up"><polyline points="23 6 13.5 15.5 8.5 10.5 1 18"></polyline><polyline points="17 6 23 6 23 12"></polyline></svg>--}}
{{--                        <span> {{trans('sidebar.prices')}} </span>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </li>--}}



{{--            <li class="menu {{ (request()->is('*/price_text*')) ? 'active' : '' }}">--}}
{{--                <a href="{{route('admin_panel.price_text.index')}}" aria-expanded="false" class="dropdown-toggle">--}}
{{--                    <div class="">--}}
{{--                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-help-circle"><circle cx="12" cy="12" r="10"></circle><path d="M9.09 9a3 3 0 0 1 5.83 1c0 2-3 3-3 3"></path><line x1="12" y1="17" x2="12.01" y2="17"></line></svg>--}}
{{--                        <span>{{trans('sidebar.prices_text')}}</span>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </li>--}}




        </ul>

    </nav>

</div>
<!--  END SIDEBAR  -->
