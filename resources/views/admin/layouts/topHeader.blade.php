@php
    $lang = App::getLocale()
@endphp
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="{{$meta->translate($lang)->keywords}}">
    <meta name="description" content="{{$meta->translate($lang)->description}}">

    <title>{{$meta->translate($lang)->title}}</title>
    <meta property="og:image" content="{{$meta->og_image}}">
    <meta property="og:title" content="{{$meta->translate($lang)->og_title}}">
    <meta property="og:description" content="{{$meta->translate($lang)->description}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta property="og:image:width" content="600" />
    <meta property="og:image:height" content="350" />
    <meta property="og:url" content="<?php echo URL::current(); ?>" />
    <meta property="og:site_name" content="LaunchPage">
    <meta property="og:type" content="website" />
    <meta property="og:updated_time" content="1440432930" />
    <link rel="shortcut icon" href="{{asset($meta->icon)}}" type="image/x-icon">

    <link rel="shortcut icon" href="{{asset($meta->icon)}}">

{{--    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">--}}
    <link href="{{ asset('css/light/loader.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/dark/loader.css') }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('js/loader.js') }}"></script>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;1000&family=Noto+Sans+Arabic:wght@300&display=swap" rel="stylesheet">

    <link href="{{ asset('css/bootstrap.rtl.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/light/plugins.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/dark/plugins.css') }}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <link href="{{ asset('css/apexcharts.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/light/dash_1.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/dark/dashboard/dash_1.css') }}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->

    <!-- Tables -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/datatables.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('css/light/table/scrollspyNav.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/dark/table/scrollspyNav.css')}}">


    <link rel="stylesheet" type="text/css" href="{{asset('css/light/table/dt-global_style.css')}}">
{{--    <link rel="stylesheet" type="text/css" href="{{asset('css/light/table/custom_dt_custom.css')}}">--}}

    <link rel="stylesheet" type="text/css" href="{{asset('css/dark/table/dt-global_style.css')}}">
{{--    <link rel="stylesheet" type="text/css" href="{{asset('css/dark/table/custom_dt_custom.css')}}">--}}


    <link rel="stylesheet" type="text/css" href="{{asset('splide/splide.min.css')}}">

{{--    <link rel="stylesheet" type="text/css" href="../src/assets/css/light/scrollspyNav.css" />--}}
    <link rel="stylesheet" type="text/css" href="{{asset('css/light/splide/custom-splide.min.css')}}">

{{--    <link rel="stylesheet" type="text/css" href="../src/assets/css/dark/scrollspyNav.css" />--}}
    <link rel="stylesheet" type="text/css" href="{{asset('css/dark/splide/custom-splide.min.css')}}">

    <!--  BEGIN CUSTOM STYLE PROFILE  -->
    <link href="{{asset('css/light/profile/list-group.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/light/profile/user-profile.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('css/dark/profile/list-group.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/dark/profile/user-profile.css')}}" rel="stylesheet" type="text/css" />
    <!--  END CUSTOM STYLE PROFILE  -->

    <!--  BEGIN CUSTOM STYLE AJAX SEARCH  -->
{{--    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <!--  END CUSTOM STYLE AJAX SEARCH  -->

    <style>
        form div{
            margin: 15px;
        }
    </style>

    <!--  Icon  -->

    <link rel="stylesheet"  href="{{asset('css/regular.css')}}">
    <link rel="stylesheet"  href="{{asset('css/fontawesome.css')}}">

    <link href="{{ asset('css/light/scrollspyNav.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/light/font-icons.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('css/dark/scrollspyNav.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/dark/font-icons.css') }}" rel="stylesheet" type="text/css" />

    <!--  e
    End Icon  -->
</head>

