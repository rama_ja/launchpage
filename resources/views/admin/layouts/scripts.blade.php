<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('js/perfect-scrollbar.min.js') }}"></script>
<script src="{{ asset('js/mousetrap.min.js') }}"></script>
<script src="{{ asset('js/waves.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<!-- END GLOBAL MANDATORY SCRIPTS -->

<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
<script src="{{ asset('js/apexcharts.min.js') }}"></script>
<script src="{{ asset('js/dash_1.js') }}"></script>
<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->



{{--<script src="{{asset('js/custom.js')}}"></script>--}}
<!-- END GLOBAL MANDATORY SCRIPTS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset('js/datatables.js')}}"></script>

<!-- Table -->
<script src="{{asset('js/table/vendors.min.js')}}"></script>
<script src="{{asset('js/table/highlight.pack.js')}}"></script>
<script src="{{asset('js/table/custom.js')}}"></script>

<script src="{{asset('js/table/scrollspyNav.js')}}"></script>
<script src="{{asset('js/scrollspyNav.js')}}"></script>

<script>
    checkall('checkbox_parent_all', 'checkbox_child');
    checkall('hover_parent_all', 'hover_child');
    checkall('striped_parent_all', 'striped_child');
    checkall('bordered_parent_all', 'bordered_child');
    checkall('mixed_parent_all', 'mixed_child');
    checkall('noSpacing_parent_all', 'noSpacing_child');
    checkall('custom_mixed_parent_all', 'custom_mixed_child');
</script>

{{--<script src="../src/plugins/src/highlight/highlight.pack.js"></script>--}}
{{--<!-- END GLOBAL MANDATORY STYLES -->--}}
{{--<script src="../src/assets/js/scrollspyNav.js"></script>--}}
<script src="{{asset('splide/splide.min.js')}}"></script>
<script src="{{asset('splide/custom-splide.js')}}"></script>

<!-- Icons -->
<script src="{{asset('js/feather.min.js')}}"></script>
<script>
    feather.replace();
</script>
<!-- End Icons -->
