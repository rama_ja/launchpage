@extends('admin.dashboard')

@section('content')

    <div id="content" class="main-content">
        <!--  BEGIN BREADCRUMBS  -->
        <div class="secondary-nav">
            <div class="breadcrumbs-container" data-page-heading="Analytics">
                <header class="header navbar navbar-expand-sm">
                    <a href="javascript:void(0);" class="btn-toggle sidebarCollapse" data-placement="bottom">
                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                    </a>
                    <div class="d-flex breadcrumb-content">
                        <div class="page-header">

                            <div class="page-title">
                            </div>

                            <nav class="breadcrumb-style-one" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active"> {{trans('sidebar.header')}} </li>

                                </ol>
                            </nav>

                        </div>
                    </div>
                </header>
            </div>
        </div>
        <br>
        <!--  END BREADCRUMBS  -->
        <div class="row layout-spacing " >

            <!-- Content -->
            <div class="col-12" style="margin:2% 2% auto;">
                <div class="user-profile ">
                    <div class="widget-content widget-content-area">
                        <div class=" " style="padding:2% 2% 0px; " >
                            <h3 class="">{{trans('sidebar.header')}}  </h3>
                        </div>

                        <div class="" style="padding: 2%;">
                            <div class="container">

                                <form class=" g-3" method="post" action="{{route('admin_panel.headers.update',1)}}" enctype="multipart/form-data" >
                                    @method('PATCH')
                                    @csrf
                                    @if ($errors->any())

                                        <div class="alert alert-danger">

                                            <ul style="list-style: none;margin:0">

                                                @foreach ($errors->all() as $error)

                                                    <li>{{ $error }}</li>

                                                @endforeach

                                            </ul>

                                        </div>

                                    @endif

                                    <div class="col-md-6">
                                        {{--                                        <label for="inputAddress" class="form-label"> الصورة الرئيسية </label>--}}
                                        <img alt="avatar" src="/{{$headers->image}}"  width="200" height="200"/>
                                        <br><br>
                                        <input type="file" class="form-control"  placeholder="1234 Main St" name="image" >
                                        <p style="color: red"> {{trans('admin.image_size')}}  1080*1920 </p>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label">{{trans('admin.title_ar')}} </label>
                                        <input type="text" class="form-control"  name="title_ar" value="{{$headers->translate('ar')->title}}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label">{{trans('admin.des_ar')}}  </label>
                                        <textarea  class="form-control"  name="description_ar" >{{$headers->translate('ar')->description}}</textarea>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label">{{trans('admin.title_en')}} </label>
                                        <input type="text" class="form-control"  name="title_en" value="{{$headers->translate('en')->title}}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label">{{trans('admin.des_en')}} </label>
                                        <textarea class="form-control"  name="description_en" >{{$headers->translate('en')->description}}</textarea>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label"> {{trans('admin.button_ar')}} </label>
                                        <input type="text" class="form-control"  name="button_name_ar" value="{{$headers->translate('ar')->button_name}}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label">{{trans('admin.button_en')}}</label>
                                        <input type="text" class="form-control"  name="button_name_en" value="{{$headers->translate('en')->button_name}}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputAddress" class="form-label"> {{trans('admin.file')}} </label>
                                        {{--                                        <img alt="avatar" src="/{{$headers->file}}"  width="200" height="200"/>--}}
                                        <br><br>
                                        <input type="file" class="form-control"  placeholder="1234 Main St" name="file" >
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label"> {{trans('admin.web')}}</label>
                                        @if($headers->button_type=='2')
                                            <input type="url" class="form-control"  name="link" value="{{$headers->button_link}}">
                                        @else
                                            <input type="url" class="form-control"  name="link" >
                                        @endif
                                    </div>
                                    <div class="col-6">
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary">{{trans('admin.edit')}}</button>
                                        </div>
                                    </div>


                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!--  BEGIN FOOTER  -->
    @include('admin.layouts.footer')
    <!--  END FOOTER  -->

    </div>



@endsection
