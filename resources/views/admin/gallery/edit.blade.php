@extends('admin.dashboard')

@section('content')

    <div id="content" class="main-content">
        <!--  BEGIN BREADCRUMBS  -->
        <div class="secondary-nav">
            <div class="breadcrumbs-container" data-page-heading="Analytics">
                <header class="header navbar navbar-expand-sm">
                    <a href="javascript:void(0);" class="btn-toggle sidebarCollapse" data-placement="bottom">
                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                    </a>
                    <div class="d-flex breadcrumb-content">
                        <div class="page-header">

                            <div class="page-title">
                            </div>

                            <nav class="breadcrumb-style-one" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item ">{{trans('sidebar.gallery')}} </li>
                                    <li class="breadcrumb-item active"> {{trans('admin.edit_image')}} </li>
                                </ol>
                            </nav>

                        </div>
                    </div>
                </header>
            </div>
        </div>
        <br>
        <!--  END BREADCRUMBS  -->
        <div class="row layout-spacing " >

            <!-- Content -->
            <div class="col-12" style="margin:2% 2% auto;">
                <div class="user-profile">
                    <div class="widget-content widget-content-area">
                        <div class=" " style="padding:2% 2% 0px; " >
                            <h3 class="">{{trans('admin.edit_image')}}</h3>
                        </div>

                        <div class="" style="padding: 2%;">
                            <div class="container">

                                <form class=" g-3" method="post" action="{{route('admin_panel.gallery.update',$item->id)}}" enctype="multipart/form-data" >
                                    @method('PATCH')
                                    @csrf
                                    @if ($errors->any())

                                        <div class="alert alert-danger">

                                            <ul style="list-style: none;margin:0">

                                                @foreach ($errors->all() as $error)

                                                    <li>{{ $error }}</li>

                                                @endforeach

                                            </ul>

                                        </div>

                                    @endif
                                    <div class="col-md-12">
                                        <label for="inputAddress" class="form-label"> {{trans('admin.image')}} </label>
                                        <input type="file" class="form-control"  placeholder="1234 Main St" name="file" >
                                        <p style="color: red"> {{trans('admin.image_size')}}  460*555 </p>
                                    </div>

                                    <div class="col-md-12">
                                        <label for="inputEmail4" class="form-label">{{trans('admin.title_ar')}}</label>
                                        <input type="text" class="form-control" value="{{$item->translate('ar')->title}}" name="title_ar">
                                    </div>

                                    <div class="col-md-12">
                                        <label for="inputEmail4" class="form-label">{{trans('admin.title_en')}} </label>
                                        <input type="text" class="form-control" value="{{$item->translate('en')->title}}" name="title_en">
                                    </div>


                                    <div class="col-12">
                                        <div class="text-center">
                                        <button type="submit" class="btn btn-primary">{{trans('admin.edit')}}</button>
                                        </div>
                                    </div>


                                </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <script>
            $(document).ready(function() {
                $('#text-en').summernote();
            });
            $(document).ready(function() {
                $('#text-ar').summernote();
            });

        </script>
        <!--  BEGIN FOOTER  -->
    @include('admin.layouts.footer')
    <!--  END FOOTER  -->

    </div>



@endsection
