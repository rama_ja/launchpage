$(window).on('load', function () {

	"use strict";


	/* ========================================================== */
	/*   Navigation Background Color                              */
	/* ========================================================== */

	$(window).on('scroll', function () {
		if ($(this).scrollTop() > 450) {
			$('.navbar-fixed-top').addClass('opaque');
		} else {
			$('.navbar-fixed-top').removeClass('opaque');
		}
	});


	/* ========================================================== */
	/*   Hide Responsive Navigation On-Click                      */
	/* ========================================================== */

	$(".navbar-nav li a").on('click', function (event) {
		$(".navbar-collapse").collapse('hide');
	});


	/* ========================================================== */
	/*   Navigation Color                                         */
	/* ========================================================== */

	$('#navbarCollapse').onePageNav({
		filter: ':not(.external)'
	});


	/* ========================================================== */
	/*   SmoothScroll                                             */
	/* ========================================================== */

	$(".navbar-nav li:not(.discover-link) a, a.scrool").on('click', function (e) {
		var full_url = this.href;
		var parts = full_url.split("#");
		var trgt = parts[1];
		var target_offset = $("#" + trgt).offset();
		var target_top = target_offset.top;
		$('html,body').animate({ scrollTop: target_top - 70 }, 1000);
		return false;

	});
	const listener = function (e) {
        e.preventDefault();
    };
    $('.navbar-nav li.discover-link a').removeEventListener("click", listener);

	/* ========================================================== */
	/*   Newsletter                                               */
	/* ========================================================== */


});



/* ========================================================== */
/*   Popup-Gallery                                            */
/* ========================================================== */
$('.popup-gallery').find('a.popup1').magnificPopup({
	type: 'image',
	gallery: {
		enabled: true
	}
});

$('.popup-gallery').find('a.popup2').magnificPopup({
	type: 'image',
	gallery: {
		enabled: true
	}
});

$('.popup-gallery').find('a.popup3').magnificPopup({
	type: 'image',
	gallery: {
		enabled: true
	}
});

$('.popup-gallery').find('a.popup4').magnificPopup({
	type: 'iframe',
	gallery: {
		enabled: false
	}
});



// countryswiper
var countryswiper = new Swiper(".countryswiper", {
	slidesPerView: 1,
	slidesPerGroup: 1,
	loop: true,
	loopFillGroupWithBlank: true,
	keyboard: {
		enabled: true,
	},
	autoplay: {
		delay: 2000,
	},
	speed: 1000,
	pagination: {
		el: ".swiper-pagination",
		clickable: true,
	},
	navigation: {
		nextEl: ".swiper-button-next",
		prevEl: ".swiper-button-prev",
	},
});


var myswiper = new Swiper(".myswiper", {
	slidesPerView: 2,
	slidesPerGroup: 1,
	loop:false,
	loopFillGroupWithBlank: true,
	keyboard: {
		enabled: true,
	},
	speed: 1000,
	pagination: {
		el: ".swiper-pagination",
		clickable: true,
	},
	navigation: {
		nextEl: ".swiper-button-next",
		prevEl: ".swiper-button-prev",
	},
	on: {
        init: function () {
            if (this.slides.length ==2) {
                // First way:
                this.allowSlidePrev = this.allowSlideNext = false; // disabling swiping
                this.el.querySelector(".swiper-button-prev").setAttribute('hidden', '');  // hiding arrows prev&next
                this.el.querySelector(".swiper-button-next").setAttribute('hidden', '');

                // Second way:
                // this.el.classList.add('swiper-no-swiping');
            }
        }
    },
	breakpoints: {
		320: {
			slidesPerView: 1,
		},
		480: {
			slidesPerView: 1,
		},
		768: {
			slidesPerView: 2,
		},
		1024: {
			slidesPerView: 2,
		}
	}
	
});
// if($(".myswiper .swiper-slide").length == 2) {
// 	$('.swiper-wrapper').addClass( "disabled" );
// 	$('.myswiper .swiper-pagination').addClass( "disabled" );
// }

//Accordion Box
if ($('.accordion-box').length) {
	$(".accordion-box").on('click', '.acc-btn', function () {

		var outerBox = $(this).parents('.accordion-box');
		var target = $(this).parents('.accordion');

		if ($(this).hasClass('active') !== true) {
			$(outerBox).find('.accordion .acc-btn').removeClass('active');
		}

		if ($(this).next('.acc-content').is(':visible')) {
			return false;
		} else {
			$(this).addClass('active');
			$(outerBox).children('.accordion').removeClass('active-block');
			$(outerBox).find('.accordion').children('.acc-content').slideUp(300);
			target.addClass('active-block');
			$(this).next('.acc-content').slideDown(300);
		}
	});
};



