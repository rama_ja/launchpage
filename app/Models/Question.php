<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    protected $fillable = [];

    public $translatedAttributes = ['title','description'];
}
