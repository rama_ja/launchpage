<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Meta extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    protected $fillable = ['header_logo','footer_logo','icon','og_image','web_color'];

    public $translatedAttributes = ['title','description','og_title','keywords','footer'];

}
