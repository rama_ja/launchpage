<?php

namespace App\Providers;

use App\Models\Meta;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*',function($view) {
            $meta=Meta::first();
            $icon=$meta->icon;
            $view->with('admin', auth()->user());
            $view->with('icon', $icon);
            $view->with('meta', $meta);
        });

        Schema::defaultStringLength(191);
    }
}
