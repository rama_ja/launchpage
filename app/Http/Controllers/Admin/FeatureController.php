<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Feature;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class FeatureController extends Controller
{
    public function index(){
        $features=Feature::orderBy('created_at','desc')->get();
        return view ('admin.features.index',['features'=>$features]);
    }

    public function create(){
        return view('admin.features.create');
    }
    public function store(Request $request){
        if($request->file != ''){
            $path = 'images/features/';


            $file = $request->file;
            $imageExtension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $imageExtension;
            $filename = $path.$filename;
            $file->move($path, $filename);



        }
        $feature=Feature::create([
            'image'      =>$filename,
            'ar'=>['title'=>$request->title_ar
                ,'description' => $request->description_ar
            ],
            'en'=>['title'=>$request->title_en
                ,'description' => $request->description_en
            ]
        ]);


        return redirect(route('admin_panel.features.index'));
    }
    public function edit(Request $request,$id){
        $feature=Feature::find($id);
        return view('admin.features.edit',['feature'=>$feature]);
    }

    public function update(Request $request,$id){
        $feature=Feature::find($id);
        if($request->file != ''){
            (File::exists($feature->image)) ? File::delete($feature->image) : Null;
            $path = 'images/features/';

            $file = $request->file;
            $imageExtension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $imageExtension;
            $filename = $path.$filename;
            $file->move($path, $filename);

            $feature->update([
                'image'      =>$filename
            ]);
        }

        $feature->update([
            'ar'=>['title'=>$request->title_ar
                ,'description' => $request->description_ar
            ],
            'en'=>['title'=>$request->title_en
                ,'description' => $request->description_en
            ]
        ]);

        return redirect(route('admin_panel.features.index'));
    }
    public function destroy($id){
        $feature=Feature::find($id);
        (File::exists($feature->image)) ? File::delete($feature->image) : Null;
        $feature->delete();
        return redirect()->back();
    }
}
