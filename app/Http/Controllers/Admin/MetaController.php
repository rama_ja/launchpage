<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Meta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class MetaController extends Controller
{
    public function index(){
//        Meta::create([
//            'header_logo'    =>'hh',
//            'footer_logo'    =>'hh',
//            'icon'    =>'hh',
//            'og_image'    =>'hh',
//            'web_color'    =>'hh',
//            'ar'=>['title'    => '$request->title_ar',
//                'description' => '$request->description_ar',
//                'keywords' => '$request->button_name_ar',
//                'footer' => '$request->button_name_ar',
//                'og_title'    => '$request->title_ar',
//            ],
//            'en'=>['title'    => '$request->title_en',
//                'description' => '$request->description_en',
//                'keywords' => '$request->button_name_en',
//                'footer' => '$request->button_name_en',
//                'og_title'    => '$request->title_ar'
//            ]
//        ]);
        $meta=Meta::first();
        return view('admin.metas.index',compact('meta'));
    }

    public function update(Request $request,$id){
        $meta=Meta::first();

        if($request->og_image != '' ){

            (File::exists($meta->og_image)) ? File::delete($meta->og_image) : Null;
            $path = 'images/metas/';

            $image = $request->og_image;
            $filename = $image->getClientOriginalName();
            $filename = $path.$filename;
            $image->move($path, $filename);

            $meta->update([
                'og_image'      =>$filename
            ]);

        }
        if($request->header_logo != '' ){

            (File::exists($meta->header_logo)) ? File::delete($meta->header_logo) : Null;
            $path = 'images/metas/';

            $image = $request->header_logo;
            $filename = $image->getClientOriginalName();
            $filename = $path.$filename;
            $image->move($path, $filename);

            $meta->update([
                'header_logo'      =>$filename
            ]);

        }  if($request->footer_logo != '' ){

            (File::exists($meta->footer_logo)) ? File::delete($meta->footer_logo) : Null;
            $path = 'images/metas/';

            $image = $request->footer_logo;
            $filename = $image->getClientOriginalName();
            $filename = $path.$filename;
            $image->move($path, $filename);

            $meta->update([
                'footer_logo'      =>$filename
            ]);

        } if($request->icon != '' ){

            (File::exists($meta->icon)) ? File::delete($meta->icon) : Null;
            $path = 'images/metas/';

            $image = $request->icon;
            $filename = $image->getClientOriginalName();
            $filename = $path.$filename;
            $image->move($path, $filename);

            $meta->update([
                'icon'      =>$filename
            ]);

        }
        $meta->update([
            'web_color'=>$request->web_color,
            'ar'=>['title'    => $request->title_ar,
                'description' => $request->description_ar,
                'keywords'    => $request->keywords_ar,
                'footer'      => $request->footer_ar,
                'og_title'      => $request->og_title_ar,
            ],
            'en'=>['title'    => $request->title_en,
                'description' => $request->description_en,
                'keywords'    => $request->keywords_en,
                'footer'      => $request->footer_en,
                'og_title'      => $request->og_title_en
            ]
        ]);

        return redirect()->back();
    }

}
