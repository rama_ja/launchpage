<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Section;
use Illuminate\Http\Request;

class SectionController extends Controller
{
    public function index(){

//        Section::create(['name'=>'home','ar'=>['title'=>'الصفحة الرئيسية'],'en'=>['title'=>'home']]);
//        Section::create(['name'=>'services','ar'=>['title'=>'الصفحة services'],'en'=>['title'=>'services']]);
//        Section::create(['name'=>'gallery','ar'=>['title'=>'الصفحة gallery'],'en'=>['title'=>'gallery']]);
//        Section::create(['name'=>'feature','ar'=>['title'=>'الصفحة feature'],'en'=>['title'=>'feature']]);
//        Section::create(['name'=>'pricing','ar'=>['title'=>'الصفحة pricing'],'en'=>['title'=>'pricing']]);
//        Section::create(['name'=>'faq','ar'=>['title'=>'الصفحة faq'],'en'=>['title'=>'faq']]);
//        Section::create(['name'=>'award','ar'=>['title'=>'الصفحة award'],'en'=>['title'=>'award']]);

        $home=Section::where('name','home')->first();
        $services=Section::where('name','services')->first();
        $gallery=Section::where('name','gallery')->first();
        $feature=Section::where('name','feature')->first();
        $pricing=Section::where('name','pricing')->first();
        $faq=Section::where('name','faq')->first();
        $award=Section::where('name','award')->first();
        return view('admin.sections.index',compact('home','services','gallery','feature','pricing','faq','award'));
    }

    public function update(Request $request,$id){
        $home=Section::where('name','home')->first();
        $services=Section::where('name','services')->first();
        $gallery=Section::where('name','gallery')->first();
        $feature=Section::where('name','feature')->first();
        $pricing=Section::where('name','pricing')->first();
        $faq=Section::where('name','faq')->first();
        $award=Section::where('name','award')->first();

        $home->update([
            'ar'=>['title'    => $request->home_ar,
            ],
            'en'=>['title'    => $request->home_en,
            ]
        ]);
        $services->update([
            'ar'=>['title'    => $request->services_ar,
            ],
            'en'=>['title'    => $request->services_en,
            ]
        ]);
        $gallery->update([
            'ar'=>['title'    => $request->gallery_ar,
            ],
            'en'=>['title'    => $request->gallery_en,
            ]
        ]);
        $feature->update([
            'ar'=>['title'    => $request->feature_ar,
            ],
            'en'=>['title'    => $request->feature_en,
            ]
        ]);
        $pricing->update([
            'ar'=>['title'    => $request->pricing_ar,
            ],
            'en'=>['title'    => $request->pricing_en,
            ]
        ]);
        $faq->update([
            'ar'=>['title'    => $request->faq_ar,
            ],
            'en'=>['title'    => $request->faq_en,
            ]
        ]);
        $award->update([
            'ar'=>['title'    => $request->award_ar,
            ],
            'en'=>['title'    => $request->award_en,
            ]
        ]);

        return redirect()->back();
    }

}
