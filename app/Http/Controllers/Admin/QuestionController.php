<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Question;
use Illuminate\Http\Request;


class QuestionController extends Controller
{
    public function index(){
        $questions=Question::orderBy('created_at','desc')->get();
        return view ('admin.questions.index',['questions'=>$questions]);
    }

    public function create(){
        return view('admin.questions.create');
    }
    public function store(Request $request){

        $question=Question::create([
            'ar'=>['title'=>$request->title_ar
                ,'description' => $request->description_ar
            ],
            'en'=>['title'=>$request->title_en
                ,'description' => $request->description_en
            ]
        ]);


        return redirect(route('admin_panel.questions.index'));
    }
    public function edit(Request $request,$id){
        $question=Question::find($id);
        return view('admin.questions.edit',['question'=>$question]);
    }

    public function update(Request $request,$id){
        $question=Question::find($id);

        $question->update([
            'ar'=>['title'=>$request->title_ar
                ,'description' => $request->description_ar
            ],
            'en'=>['title'=>$request->title_en
                ,'description' => $request->description_en
            ]
        ]);

        return redirect(route('admin_panel.questions.index'));
    }
    public function destroy($id){
        $question=Question::find($id);
        $question->delete();
        return redirect()->back();
    }
}
