<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class MediaController extends Controller
{
    public function edit(Request $request,$id){
        $media=Media::first();
        return view('admin.media',['media'=>$media]);
    }


    public function update(Request $request,$id){
        $media=Media::first();
        $media->update([
            'facebook'       =>$request->facebook,
            'pinterest'      =>$request->pinterest,
            'twitter'        =>$request->twitter,
            'instagram'      =>$request->instagram,
            'skype'          =>$request->skype
        ]);
        $med=Media::last();

        if($request->facebook_icon != ''){
            (File::exists($med->facebook)) ? File::delete($med->facebook) : Null;
            $path = 'images/media/';
            $file = $request->facebook_icon;
            $imageExtension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $imageExtension;
            $filename = $path.$filename;
            $file->move($path, $filename);
            $med->update([
                'facebook'       =>$filename,
            ]);
        }

        if($request->pinterest_icon != ''){
            (File::exists($med->pinterest)) ? File::delete($med->pinterest) : Null;
            $path = 'images/media/';
            $file = $request->pinterest_icon;
            $imageExtension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $imageExtension;
            $filename = $path.$filename;
            $file->move($path, $filename);
            $med->update([
                'pinterest'       =>$filename,
            ]);
        }
        if($request->twitter_icon != ''){
            (File::exists($med->twitter)) ? File::delete($med->twitter) : Null;
            $path = 'images/media/';
            $file = $request->twitter_icon;
            $imageExtension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $imageExtension;
            $filename = $path.$filename;
            $file->move($path, $filename);
            $med->update([
                'twitter'       =>$filename,
            ]);
        }
        if($request->instagram_icon != ''){
            (File::exists($med->instagram)) ? File::delete($med->instagram) : Null;
            $path = 'images/media/';
            $file = $request->instagram_icon;
            $imageExtension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $imageExtension;
            $filename = $path.$filename;
            $file->move($path, $filename);
            $med->update([
                'instagram'       =>$filename,
            ]);
        }
        if($request->skype_icon != ''){
            (File::exists($med->skype)) ? File::delete($med->skype) : Null;
            $path = 'images/media/';
            $file = $request->skype_icon;
            $imageExtension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $imageExtension;
            $filename = $path.$filename;
            $file->move($path, $filename);
            $med->update([
                'skype'       =>$filename,
            ]);
        }
        return redirect()->back();
    }

    public function editWhatsapp(){
        $media=Media::first();
        return view('admin.whatsapp',['media'=>$media]);
    }


    public function updateWhatsapp(Request $request){
        $media=Media::first();
        $media->update([
            'whatsapp'       =>$request->whatsapp
        ]);

        return redirect()->back();
    }

    public function editCall(){
        $media=Media::first();
        return view('admin.call',['media'=>$media]);
    }


    public function updateCall(Request $request){
        $media=Media::first();
        $media->update([
            'call'           =>$request->call,
        ]);

        return redirect()->back();
    }
    public function editMail(){
        $media=Media::first();
        return view('admin.mail',['media'=>$media]);
    }


    public function updateMail(Request $request){
        $media=Media::first();
        $media->update([
            'email'          =>$request->email
        ]);


        return redirect()->back();
    }
    public function editPhone(){
        $media=Media::first();
        return view('admin.phone',['media'=>$media]);
    }


    public function updatePhone(Request $request){
        $media=Media::first();
        $media->update([
            'phone'          =>$request->phone
        ]);


        return redirect()->back();
    }
}

